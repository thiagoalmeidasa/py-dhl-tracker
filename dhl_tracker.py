import logging
import traceback

from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

FORMAT = '%(levelname)s: %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)

logger = logging.getLogger(__name__)

options = Options()
options.headless = True

trackingnumber = 'YOUTRACKNUMBER'

dhl_url = 'https://webtrack.dhlglobalmail.com/?trackingnumber={}'.format(
    trackingnumber)


def track(trackingnumber):
    delay = 5  # seconds

    try:
        with webdriver.Firefox(options=options) as browser:

            logger.info("Starting to get tracing info")
            browser.get(dhl_url)

            timeline = WebDriverWait(browser, delay).until(
                EC.presence_of_element_located((By.CLASS_NAME,
                                                'timeline-last')))
            timeline_last_date = WebDriverWait(browser, delay).until(
                EC.presence_of_element_located((By.CLASS_NAME,
                                                'timeline-date')))
            logger.info("Last status update at: {} | Status: {}".format(
                timeline_last_date.text, timeline.text.replace('\n', '')))
    except (TimeoutException, WebDriverException):
        logger.error("Error getting info: {}".format(traceback.format_exc()))


if __name__ == "__main__":
    track(trackingnumber=trackingnumber)
