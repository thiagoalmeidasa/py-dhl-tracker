# Py-dhl-tracker

<!-- vim-markdown-toc GitLab -->

* [Introduction](#introduction)
* [Setup](#setup)
* [Running](#running)
* [Todo](#todo)

<!-- vim-markdown-toc -->

## Introduction

A simple page scrapper to get tracking info about some item on DHL tracking
page.

## Setup

- Requirements:

```
firefox
selenium python bindings
```

- Install python packages with [pipenv](https://pipenv.readthedocs.io/en/latest/)

```bash
pipenv install
```

## Running

- Run with pipenv:

```bash
pipenv run track
```

## Todo

- [ ] Improve tracking number input management. [create an issue](https://gitlab.com/thiagoalmeidasa/py-dhl-tracker/issues/new)
- [ ] Add tests. [create an issue](https://gitlab.com/thiagoalmeidasa/py-dhl-tracker/issues/new)
- [ ] Add cli parameters support. [create an issue](https://gitlab.com/thiagoalmeidasa/py-dhl-tracker/issues/new)
- [ ] Add option to show all history about an item instead of just the last
      update. [create an issue](https://gitlab.com/thiagoalmeidasa/py-dhl-tracker/issues/new)
